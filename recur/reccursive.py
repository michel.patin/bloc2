BLACK = 0
RED = 2
WHITE = 1
grille = [[WHITE for _ in range(6)] for _ in range(6) ]

def even(n):
    '''
    Cette fonction boucle de façon 'infini' sauf pour 0 ou True est renvoyé.
    
    '''
    if n == 0:
       return True 
    else:
       return not odd(n)

def odd(n):
    '''
    Cette fonction boucle de façon 'infini' sauf pour 0 ou True est renvoyé.
    
    '''
    if n == 0:
       return False 
    else: 
       return not even(n)

def add(a, b) :
    
    '''
    La fonction add permet de calculer la somme de deux entiers naturels.
    le principe de récursivité est ici utilisé.
    
    @parametres :
            a est une entier naturel <int>
            b est une entier naturel <int>
            
    @return :
            La somme de deux entiers naturels <int>.
            
    @exemples :
            >>> add(5, 10)
            15
            >>> add(0, 60)
            60
    '''
    
    if not b :
        return a
    else :
        return add(a+1, b-1)

def mult(a, b) :
    
    '''
    La fonction mult permet de calculer le produit de deux entiers naturels.
    le principe de récursivité est ici utilisé.
    
    @parametres :
            a est une entier naturel <int>
            b est une entier naturel <int>
            
    @return :
            Le produit de deux entiers naturels <int>.
            
    @exemples :
            >>> mult(5, 10)
            50
            >>> mult(0, 60)
            0
            >>> mult(0, 0)
            0
    '''
    
    if not b :
        return 0
    else :
        return mult(a, b-1) + a

def power(a, b) :
    
    '''
    La fonction power permet de calculer a**b où a et b sont deux entiers naturels.
    le principe de récursivité est ici utilisé.
    
    @parametres :
            a est une entier naturel <int>
            b est une entier naturel <int>
            
    @return :
            a**b <int>.
            
    @exemples :
            >>> power(5, 2)
            25
            >>> power(0, 0)
            1
            >>> power(5, 0)
            1
    '''
    
    if not b :
        return 1
    else :
        return power(a, b-1)*a
    
def euclide(a, b) :
    
    '''
    La fonction euclide permet de calculer le PGCD de deux entiers naturels a et b.
    le principe de récursivité est ici utilisé.
    
    @parametres :
            a est une entier naturel <int>
            b est une entier naturel <int>
            
    @return :
            le PGCD de deux entiers naturels a et b <int>.
            
    @exemples :
            >>> euclide(5, 2)
            1
            >>> try :
            ...   euclide(0, 0)
            ... except :
            ...     print("Le PGCD de 0 et 0 n'est pas possible")
            Le PGCD de 0 et 0 n'est pas possible
    '''
    
    try :
        if not a%b :
            return b
        else :
            return euclide(b, a%b)
    except :
        raise ZeroDivisionError("Le PGCD de 0 et de 0 n'existe pas!!!!!")

def get_coloriage(x,y) :
    return grille[x][y]

def set_coloriage(x,y, color) :
    grille[x][y] = color

def coloriage(x,y) :
    
    '''
    La fonction coloriage permet de remplir l'intérieur d'une ligne fermée en rouge
    '''
    set_coloriage(x,y, RED)
    
    if  (x-1)>= 0 and get_coloriage(x-1,y) != BLACK and get_coloriage(x-1,y) != RED :
         coloriage(x-1,y)
    if (x+1)<= len(grille)-1 and grille[x+1][y] and get_coloriage(x+1,y) != BLACK and get_coloriage(x+1,y) != RED :
         coloriage(x+1,y)
    if (y-1)>= 0 and get_coloriage(x,y-1) != BLACK and get_coloriage(x,y-1) != RED :
         coloriage(x,y-1)
    if (y+1)<= len(grille[0])-1 and  get_coloriage(x,y+1) != BLACK and get_coloriage(x,y+1) != RED:
         coloriage(x,y+1)
         
def exempleGrille() :
    set_coloriage(0,2, BLACK)
    set_coloriage(0,3, BLACK)
    set_coloriage(0,4, BLACK)
    set_coloriage(1,1, BLACK)
    set_coloriage(1,4, BLACK)
    set_coloriage(2,1, BLACK)
    set_coloriage(2,5, BLACK)
    set_coloriage(3,5, BLACK)
    set_coloriage(3,1, BLACK)
    set_coloriage(4,1, BLACK)
    set_coloriage(4,2, BLACK)
    set_coloriage(4,3, BLACK)
    set_coloriage(4,4, BLACK)
         
def afficher_grille(grille):
    for i in range(len(grille)) :
        for j in range(len(grille[0])) :
            print(grille[i][j], end="")
            print("  ", end="")
        print()
        print()
        
if __name__ == "__main__" :
    import doctest
    doctest.testmod()    