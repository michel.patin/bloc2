[select]: images/tri_selection.png
[insert]: images/tri_insert.png
[rapide]: images/tri_rapide.png
[fusion]: images/tri_fusion.png
[sort]: images/tri_sort_croissante.png
[selectD]: images/tri_selection_decroissante.png
[insertD]: images/tri_insert_decroissante.png
[rapideD]: images/tri_rapide_decroissante.png
[fusionD]: images/tri_fusion_decroissante.png
[sortD]: images/tri_sort_decroissante.png

# Analyse des tris

## Les courbes de tris(croissant).

|Tri sélection|Tri insertion|Tri rapide|Tri fusion|
|-------------|-------------|-------------|-------------|
| ![][select]|![][insert]|![][rapide]|![][fusion]|

Les courbes suivantes représentent le temps en secondes nécessaire pour trier 
une liste dans l'ordre croissant en fonction de la taille de la liste.

- Réaliser une comparaison de ces différents tris.


## Les courbes de tris(décroissant).

|Tri sélection|Tri insertion|Tri rapide|Tri fusion|
|-------------|-------------|-------------|-------------|
| ![][selectD]|![][insertD]|![][rapideD]|![][fusionD]|

Les courbes suivantes représentent le temps en secondes nécessaire pour trier 
une liste dans l'ordre croissant en fonction de la taille de la liste.

- Réaliser une comparaison de ces différents tris.
- Avec les tris ci-dessous, quelle analyse pouvez-vous faire?

## La fonction ```sort``` de python.

Comment utiliser cette fonction :

```python
    >>> l = [4,1,7,3,2,9,6,8,5]
    >>> l.sort()
    >>> l
    [1, 2, 3, 4, 5, 6, 7, 8, 9]
```

|Tri ```sort``` croissant|Tri ```sort``` décroissant|
|-------------|-------------|
| ![][sort]|![][sortD]|

Les courbes suivantes représentent le temps en secondes nécessaire pour trier 
une liste dans l'ordre croissant en fonction de la taille de la liste.

- Etudier la fonction de tri ```sort``` en la comparant avec les quatre autres tris étudiés.



