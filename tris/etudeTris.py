from timeit import timeit
import pylab

def  temps_tri_selection_longueur_donnee(longueur_liste) :
    
    '''
    Écrire le code d'une fonction Python à un paramètre, une longeur de
    liste donnée, qui renvoie la mesure de temps pour des listes de
    cette longueur
    '''

    return timeit(setup='from tris import tri_select; from listes import cree_liste_melangee',
           stmt=f'tri_select(cree_liste_melangee({longueur_liste}))',
           number=100)/100

def  temps_tri_selection_longueur_maximum(longueur_liste_maximale) :

    '''
    Modifier cette fonction pour qu'elle accepte maintenant comme
    paramètre une longeur maximale de liste, et renvoie la
    mesure de temps pour des listes de toutes les longueurs comprises
    entre 1 et cette valeur maximale. Le premier élément est le temps
    pour des listes de taille 1, le deuxième pour des listes de taille
    2, etc.

    '''
    
    return [timeit(setup='from tris import tri_select; from listes import cree_liste_melangee',
           stmt=f'tri_select(cree_liste_melangee({i}))',
           number=100)/100  for i in range(1,longueur_liste_maximale+1) ]


def   courbe_temps_execution_tri_selection(longueur_liste_maximale)  :
    
    '''
    Écrire une fonction, à un paramètre, qui produit une courbe du temps d'exécution du tri sélection
    pour des listes mélangées dont la taille varie de 1 à la valeur du paramètre donné.
    '''
    x = [i for i in range(1, longueur_liste_maximale + 1)]
    y = temps_tri_selection_longueur_maximum(longueur_liste_maximale)
    pylab.plot(y)
    pylab.title('Temps du tri par sélection')
    pylab.xlabel('taille des listes')
    pylab.ylabel('temps en secondes')
    pylab.grid()
    pylab.show()
    
def  temps_tri_longueur_maximum(longueur_liste_maximale, tri, compare) :
   
    return [timeit(setup=f'from tris import {tri}, {compare}; from listes import cree_liste_melangee',
           stmt=f'{tri}(cree_liste_melangee({i}), {compare})',
           number=100)/100  for i in range(1,longueur_liste_maximale+1) ]
    
    
def   courbe_tri_temps_execution(longueur_liste_maximale, tri, compare)  :
    
    x = [i for i in range(1, longueur_liste_maximale + 1)]
    y = temps_tri_longueur_maximum(longueur_liste_maximale, tri, compare)
    pylab.plot(y)
    pylab.title(f'Temps du tri {tri}')
    pylab.xlabel('taille des listes')
    pylab.ylabel('temps en secondes')
    pylab.grid()
    pylab.show()
    
def  temps_sort_longueur_maximum(longueur_liste_maximale, reverse) :
   
    return [timeit(setup='from listes import cree_liste_melangee',
           stmt=f'(cree_liste_melangee({i}).sort(reverse = {reverse}))',
           number=100)/100  for i in range(1,longueur_liste_maximale+1) ]
    
    
def   courbe_sort_temps_execution(longueur_liste_maximale, reverse)  :
    
    x = [i for i in range(1, longueur_liste_maximale + 1)]
    y = temps_sort_longueur_maximum(longueur_liste_maximale, reverse)
    pylab.plot(y)
    pylab.title(f'Temps du tri sort de python')
    pylab.xlabel('taille des listes')
    pylab.ylabel('temps en secondes')
    pylab.grid()
    pylab.show()

